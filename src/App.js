import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import logo from './assets/img/vickys-logo.svg'
import logobanner from './assets/img/vickys-hijos-mezcla-logo.png'
import lata1 from './assets/img/vickys_chamoy.png'
import lata2 from './assets/img/vickys_mango.png'
import lata3 from './assets/img/vickys_chelada.png'
import cinta from './assets/img/cinta1.png'
import direct1 from './assets/img/direct-family.jpg'
import direct2 from './assets/img/direct-chamoy.jpg'
import direct3 from './assets/img/direct-mango.jpg'
import direct4 from './assets/img/direct-chelada.jpg'

function showImages(el) {
  var windowHeight = window.height();
  let a = document.getElementById('latas')
  el.each(function(){
      var thisPos = this.offset().top;

      var topOfWindow = window.scrollTop();
      if (topOfWindow + windowHeight - 200 > thisPos ) {
          a.addClass("bg-blue");
      }
  });
}
window.scroll(function() {
      showImages('latas');
});

function App() {
  return (
    <div className="App">
      <div className="container-fluid bg-dark">
        <div className="container">
        <nav className="navbar navbar-light">
          <a className="navbar-brand" href="#">
          <img src={logo}/></a>
          <ul className="navbar-nav">
            <li className="nav-item">
              <a className="nav-link active" href="#">HOME</a>
            </li>
          </ul>
        </nav>
        </div>
      </div>
      <div className="container-fluid banner bg-blue">
        <div className="container">
            <div className="row">
              <div className="col-6 mt-5 text-center">
                <div className="p-2 bg-blue">
                  <img src={logobanner} className="w-75 mx-auto logobanner"/>
                </div>                
                <div className="w-75 mx-auto py-4 bg-dark text-center position-relative my-5">
                  <div className="cinta cinta1">
                    <img src={cinta}/>
                  </div>
                  <div className="cinta cinta2">
                    <img src={cinta}/>
                  </div>
                  <h3 className="text-white">NUEVA IMAGEN<br/>NUEVO SABOR</h3>
                  <div className="cinta cinta3">
                    <img src={cinta}/>
                  </div>
                  <div className="cinta cinta4">
                    <img src={cinta}/>
                  </div>
                </div>
              </div>
              <div className="col-6 d-flex justify-content-center align-items-center">
                  <div className="row latas" id="latas">
                    <div className="col-4">
                      <img src={lata1} className="w-100 mx-auto"/>
                    </div>
                    <div className="col-4">
                      <img src={lata2} className="w-100 mx-auto"/>
                    </div>
                    <div className="col-4">
                      <img src={lata3} className="w-100 mx-auto"/>
                    </div>
                  </div>
              </div>
            </div>
        </div>
      </div>
      <div className="container-fluid">
        <div className="row py-4 bg-blue">
          <div className="col-3 px-4">
            <div className="direct">
              <img src={direct1} className="w-100 direct-img"/>
              <div className="text-direct">
                <h5 className="text-white">Para un hijo de la mezcla,😎 tres sabores rifados:  Vicky Chamoy, Vicky Mango y Vicky Chelada.</h5>
              </div>
            </div>
          </div>
          <div className="col-3 px-4">
            <div className="direct">
              <img src={direct2} className="w-100 direct-img"/>
              <div className="text-direct">
                <h5 className="text-white">¡Pásele! ¡Pásele! Ya llegó la ¡Vicky Chamoy! 🍺 🔥</h5>
              </div>
            </div>
          </div>
          <div className="col-3 px-4">
            <div className="direct">
              <img src={direct3} className="w-100 direct-img"/>
              <div className="text-direct">
                <h5 className="text-white bt-4">Le venimos manejando lo que viene siendo la sabrosa ¡Vicky Mango! ⚡ </h5>
              </div>
            </div>
          </div>
          <div className="col-3 px-4">
            <div className="direct">
              <img src={direct4} className="w-100 direct-img"/>
              <div className="text-direct">
                <h5 className="text-white">Del barrio pa'l mundo, le traemos la ¡Vicky Chelada!💥👌🏽</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
